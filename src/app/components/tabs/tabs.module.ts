import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TabsComponent } from './tabs.component';
import { IonicModule } from '@ionic/angular';


const routes: Routes = [
  {
    path: '',
    component: TabsComponent,
    children:[
      {
        path:'home',
        loadChildren:'../pages/home/home.module#HomePageModule'
      }
    ]
  }
];

@NgModule({
  declarations: [TabsComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes)
  ]
})
export class TabsModule { }
