import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'muro', pathMatch: 'full' },
  { path: 'muro', loadChildren: './pages/muro/muro.module#MuroPageModule' },
  { path: 'comunicaciones', loadChildren: './pages/comunicaciones/comunicaciones.module#ComunicacionesPageModule' },
  { path: 'apoderado', loadChildren: './pages/apoderado/apoderado.module#ApoderadoPageModule' },
  { path: 'detalle', loadChildren: './pages/comunicaciones/detalle/detalle.module#DetallePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
