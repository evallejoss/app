import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalDetallePage } from '../../modal-detalle/modal-detalle.page';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  constructor( private modalCtrl : ModalController) { }

  ngOnInit() {
  }

  async abrirModal(){
    const modal = await this.modalCtrl.create({
      component: ModalDetallePage
    });

    return await modal.present();
    // const { data } = await modal.onDidDismiss();
    // console.log(data);
  }

}
